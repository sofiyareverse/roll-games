Rails.application.routes.draw do
  devise_for :users, path: 'users', controllers: { sessions: 'users/sessions', omniauth_callbacks: 'users/omniauth_callbacks' }

  resources :articles do
    resources :comments
  end

  root 'welcome#index'
end
