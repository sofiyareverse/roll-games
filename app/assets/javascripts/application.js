//= require jquery
//= require jquery_ujs
//= require rails-ujs
//= require turbolinks
//= require_tree .

$(document).on("turbolinks:load", function() {
    $('.redactor').redactor({
        focus: true,
        fileUpload: '/file-upload.php',
        imageUpload: '/modules/upload.php'
    });
});

